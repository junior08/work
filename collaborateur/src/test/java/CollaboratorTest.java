import com.mmdzim.service.collaborator.domain.collaborator.dto.CreatedCollaboratorDTO;
import com.mmdzim.service.collaborator.domain.collaborator.entity.Collaborator;
import com.mmdzim.service.collaborator.domain.collaborator.exception.CollaboratorNotFoundException;
import com.mmdzim.service.collaborator.domain.collaborator.mapper.CollaboratorMapper;
import com.mmdzim.service.collaborator.domain.collaborator.repository.CollaboratorRepository;
import com.mmdzim.service.collaborator.domain.collaborator.service.ServiceCollaborator;
import com.mmdzim.service.collaborator.domain.collaborator.service.impl.ImplServiceCollaborator;
import com.mmdzim.service.collaborator.domain.poste.entity.Profile;
import com.mmdzim.service.collaborator.domain.poste.service.ServiceProfile;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class CollaboratorTest {
    @Mock
    private CollaboratorRepository collaboratorRepository;
    @Mock
    private ServiceProfile serviceProfile;
    @InjectMocks
    private ImplServiceCollaborator collaboratorService;


    @Test
    public void testAddCollaborator(){
        CreatedCollaboratorDTO collaboratorDTO = new CreatedCollaboratorDTO();
        collaboratorDTO.setFirstName("John");
        collaboratorDTO.setLastName("Doe");
        collaboratorDTO.setBirthday("01/01/1990");
        collaboratorDTO.setNationality("American");
        collaboratorDTO.setSocialSecurityNumber("123456789");
        collaboratorDTO.setRegion("New York");
        collaboratorDTO.setPostalCode("10001");
        collaboratorDTO.setAddress("123 Main St");
        //collaboratorDTO.setProfile(new ProfileDTO());

        Collaborator collaboratorEntity = new Collaborator();
        collaboratorEntity.setId(1L);
        collaboratorEntity.setFirstName("John");
        collaboratorEntity.setLastName("Doe");
        collaboratorEntity.setBirthday("01/01/1990");
        collaboratorEntity.setNationality("American");
        collaboratorEntity.setSocialSecurityNumber("123456789");
        collaboratorEntity.setRegion("New York");
        collaboratorEntity.setPostalCode("10001");
        collaboratorEntity.setAddress("123 Main St");
        collaboratorEntity.setAvailable(true);
        collaboratorEntity.setProfile(new Profile());

        CollaboratorRepository collaboratorRepository = mock(CollaboratorRepository.class);
        when(collaboratorRepository.save(any(Collaborator.class))).thenReturn(collaboratorEntity);

        CollaboratorMapper collaboratorMapper = mock(CollaboratorMapper.class);
        when(collaboratorMapper.collaboratorDtoToCollaboratorEntity(any(CreatedCollaboratorDTO.class), null)).thenReturn(collaboratorEntity);

        // Initiate Service
        ServiceCollaborator serviceCollaborator = new ImplServiceCollaborator(collaboratorRepository, collaboratorMapper, null, null, null);
        Collaborator result = serviceCollaborator.createCollaborator(collaboratorDTO);

        assertNotNull(result);
        assertEquals("John", result.getFirstName());
        assertEquals("Doe", result.getLastName());


    }
    @Test
    public void testGetOneCollaborator(){
        Collaborator collaborator = new Collaborator();
        collaborator.setId(2L);
        collaborator.setFirstName("Khazim");
        collaborator.setLastName("Ndiaye");
        CollaboratorRepository repository = mock(CollaboratorRepository.class);
        when(repository.findById(2L)).thenReturn( Optional.of(collaborator));
        ServiceCollaborator serviceCollaborator = new ImplServiceCollaborator(repository, null, null, null, null);
        Collaborator result = serviceCollaborator.getOneCollaborator(2L);
        assertNotNull(result);
        assertEquals(2L, result.getId().longValue());
        assertEquals("Khazim", result.getFirstName());
        assertEquals("Ndiaye", result.getLastName());
    }
    @Test
    public void testNotFoundCollaborator(){
       CollaboratorRepository repository = mock(CollaboratorRepository.class);
       when(repository.findById(12L)).thenReturn(Optional.empty());
       ServiceCollaborator serviceCollaborator = new ImplServiceCollaborator(repository, null, null, null, null);
       assertThrows(CollaboratorNotFoundException.class, () -> serviceCollaborator.getOneCollaborator(12L));

    }
    @Test
    public void testFindByProfil(){
        String profilName = "Java";
        List<Collaborator> collaborators = new ArrayList<>();
        Collaborator collaborator1 = new Collaborator();
        collaborator1.setId(2L);
        collaborator1.setFirstName("Aly");
        collaborator1.setLastName("Toure");
        collaborators.add(collaborator1);
        Collaborator collaborator2 = new Collaborator();
        collaborator2.setId(3L);
        collaborator2.setFirstName("Khazim");
        collaborator2.setLastName("Ndiaye");
        collaborators.add(collaborator2);
        Collaborator collaborator3 = new Collaborator();
        collaborator3.setId(4L);
        collaborator3.setFirstName("Mame mariama");
        collaborator3.setLastName("Diagne");
        collaborators.add(collaborator3);

       CollaboratorRepository repository = mock(CollaboratorRepository.class);
       when(repository.getCollaboratorByProfileName(profilName)).thenReturn(Optional.of(collaborators));

       ServiceCollaborator serviceCollaborator = new ImplServiceCollaborator(repository, null, null, null, null);
       List<Collaborator> result = serviceCollaborator.findByProfil(profilName);
       assertEquals(3, result.size());
    }


    @Test
    public void testDeletedCollaborator(){
        Long id = 2L;
        Collaborator collaborator = new Collaborator();
        collaborator.setId(id);

        when(collaboratorRepository.findById(id)).thenReturn(Optional.of(collaborator));

        collaboratorService.deleteCollaborator(id);

        verify(collaboratorRepository, times(1)).deleteById(id);
    }

    @Test
    public  void testFindAll(){
        List<Collaborator> expected = new ArrayList<>();
        Collaborator c1 = new Collaborator();
        c1.setId(2L);
        expected.add(c1);
        Collaborator c2 = new Collaborator();
        c2.setId(4L);
        expected.add(c2);
        Collaborator c3 = new Collaborator();
        c3.setId(3L);
        expected.add(c3);
        when(collaboratorRepository.findAll()).thenReturn(expected);
        List<Collaborator> list = collaboratorService.findAll();
        assertEquals(expected.size(), list.size());
    }

    @Test
    public void testPaginationListCollaborator(){
        Pageable pageable = PageRequest.of(0, 10);

        // Create a mock page of collaborators
        List<Collaborator> collaborators = new ArrayList<>();
        Collaborator collaborator1 = new Collaborator();
        collaborator1.setId(1L);
        collaborator1.setFirstName("John");
        collaborator1.setLastName("Doe");
        collaborators.add(collaborator1);
        Collaborator collaborator2 = new Collaborator();
        collaborator2.setId(2L);
        collaborator2.setFirstName("Jane");
        collaborator2.setLastName("Doe");
        collaborators.add(collaborator2);
        Page<Collaborator> page = new PageImpl<>(collaborators);

        // Create a mock collaborator repository
        CollaboratorRepository collaboratorRepository = mock(CollaboratorRepository.class);
        when(collaboratorRepository.findAll(any(Pageable.class))).thenReturn(page);

        // Create a collaborator service with the mock repository
        ServiceCollaborator serviceCollaborator = new ImplServiceCollaborator(collaboratorRepository, null, null, null, null);

        // Call the listCollaborator method and verify the result
        Page<Collaborator> result = serviceCollaborator.listCollaborator(pageable);
        assertNotNull(result);
        assertEquals(2, result.getTotalElements());
        assertEquals("John", result.getContent().get(0).getFirstName());
        assertEquals("Doe", result.getContent().get(0).getLastName());
        assertEquals("Jane", result.getContent().get(1).getFirstName());
        assertEquals("Doe", result.getContent().get(1).getLastName());
    }

    @Test
    public void testUpdateProfileCollaborator(){
        Collaborator collaborator = new Collaborator();
        collaborator.setId(2L);
        Profile profile = new Profile();
        profile.setId(2L);
        ServiceCollaborator serviceCollaborator = new ImplServiceCollaborator(collaboratorRepository, null, serviceProfile, null, null);
        when(collaboratorRepository.findById(2L)).thenReturn(Optional.of(collaborator));
        when(serviceProfile.findById(2L)).thenReturn(profile);
        serviceCollaborator.addOrUpdatePoste(collaborator.getId(), profile.getId());
        assertEquals(profile.getId(), collaborator.getProfile().getId());
    }



}
