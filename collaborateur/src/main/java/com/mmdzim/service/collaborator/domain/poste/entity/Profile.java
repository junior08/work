package com.mmdzim.service.collaborator.domain.poste.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mmdzim.service.collaborator.domain.collaborator.entity.Collaborator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import javax.persistence.*;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Profile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @OneToMany(mappedBy = "profile", fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Collaborator> collaborators;
    private Profile(Builder builder) {
        id = builder.id;
        name = builder.name;
        collaborators = builder.collaborators;
    }

    public static final class Builder {
        private Long id;
        private String name;
        private List<Collaborator> collaborators;

        private Builder() {
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder id(Long val) {
            id = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder collaborators(List<Collaborator> val) {
            collaborators = val;
            return this;
        }

        public Profile build() {
            return new Profile(this);
        }
    }
}
