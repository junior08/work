package com.mmdzim.service.collaborator.domain.collaborator.mapper;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mmdzim.service.collaborator.domain.collaborator.entity.Competences;
import com.mmdzim.service.collaborator.domain.collaborator.exception.CollaboratorException;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.io.IOException;
import java.util.List;


@Converter(autoApply = true)
@Slf4j
public class CompetenceConverter  implements AttributeConverter<List<Competences>, String> {
   private static  final ObjectMapper objectMapper = new ObjectMapper();
    @Override
    public String convertToDatabaseColumn(List<Competences> competencesList) {
        String competencesInfoJson = null;
        try {
            competencesInfoJson = objectMapper.writeValueAsString(competencesList);
        } catch (final JsonProcessingException e) {
            log.error("JSON error: {}", e);
        }
        return competencesInfoJson;
    }

    @Override
    public List<Competences> convertToEntityAttribute(String competence) {
        List<Competences> list = null;
        try{
            list = objectMapper.readValue(competence, new TypeReference<List<Competences>>() {});
        } catch (final IOException e) {
           log.error("JSON error: {}", e);
        }
        return  list;
    }
}