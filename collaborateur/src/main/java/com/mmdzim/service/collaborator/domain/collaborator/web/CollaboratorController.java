package com.mmdzim.service.collaborator.domain.collaborator.web;


import com.mmdzim.service.collaborator.domain.collaborator.dto.AddSkillDTO;
import com.mmdzim.service.collaborator.domain.collaborator.dto.CreatedCollaboratorDTO;
import com.mmdzim.service.collaborator.domain.collaborator.dto.RequestAddSkillsDTO;
import com.mmdzim.service.collaborator.domain.collaborator.exception.CollaboratorException;
import com.mmdzim.service.collaborator.domain.collaborator.helper.ResponseQuery;
import com.mmdzim.service.collaborator.domain.collaborator.service.ServiceCollaborator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.UUID;

@RequestMapping("api/collaborator")
@RestController
@Slf4j
public class CollaboratorController {
    private final ServiceCollaborator serviceCollaborator;
    public CollaboratorController(ServiceCollaborator serviceCollaborator) {
        this.serviceCollaborator = serviceCollaborator;
    }

    @PostMapping("/")
    public ResponseEntity<ResponseQuery> create(@RequestBody CreatedCollaboratorDTO collaboratorDTO){
        UUID uuid = UUID.randomUUID();
        log.info("Operation of creation on collaborator with id:{} ", uuid);
        try{
            return ResponseEntity.ok(
                    ResponseQuery.builder()
                            .message("Success")
                            .code(HttpStatus.ACCEPTED.value())
                            .status(HttpStatus.ACCEPTED)
                            .data(Map.of("Collaborator creation Operation: ", serviceCollaborator.createCollaborator(collaboratorDTO)))
                            .build()
            );
        }catch (CollaboratorException e){
            return ResponseEntity.ok(
                    ResponseQuery.builder()
                            .message("An error has been encountered")
                            .code(HttpStatus.BAD_REQUEST.value())
                            .status(HttpStatus.BAD_REQUEST)
                            .data(Map.of("An error has been encountered: ", e))
                            .build()
            );
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseQuery> getOneCollaborator(@PathVariable("id") Long id){
        log.info("GET ONE COLLABORATOR");
        return ResponseEntity.ok(
               ResponseQuery.builder()
                       .message("Message")
                       .status(HttpStatus.OK)
                       .code(HttpStatus.OK.value())
                       .data(Map.of("Get one Collaborator", serviceCollaborator.getOne(id)))
                .build()
        );
    }
    @GetMapping("/list/{page}/{size}")
    public ResponseEntity<ResponseQuery>
            listCollaboratorPaginated(@PathVariable("page") int page, @PathVariable("size") int size) throws CollaboratorException{
        try {
            Pageable pageRequest = PageRequest.of(page, size);
            log.info("Pagination withe page: {}, and size: {}", page, size);
            return ResponseEntity.ok(
                    ResponseQuery.builder()
                            .status(HttpStatus.ACCEPTED)
                            .code(HttpStatus.ACCEPTED.value())
                            .message("Returned Data paginated page :"+ page+ " size: "+size)
                            .data(Map.of("List", serviceCollaborator.listCollaborator(pageRequest)))
                            .build()
            );
        }catch (CollaboratorException e) {
            throw new CollaboratorException("Error encountered: "+e.getMessage()+ " caused: ", e.getCause());
        }
    }
    @GetMapping("/")
    public ResponseEntity<ResponseQuery> getAll(){
        try {
            return ResponseEntity.ok(
                    ResponseQuery.builder()
                            .status(HttpStatus.ACCEPTED)
                            .code(HttpStatus.ACCEPTED.value())
                            .message("Returned all data")
                            .data(Map.of("List", serviceCollaborator.findAll()))
                            .build()
            );
        } catch (CollaboratorException e) {
            throw new CollaboratorException("Error encountered: "+e.getMessage()+ " caused: ", e.getCause());
        }
    }

    @PostMapping("/add-skills")
    @Transactional
    public  ResponseEntity<ResponseQuery> addSkills(@RequestBody AddSkillDTO skillDTO) {
       try {
           return ResponseEntity.ok(
                   ResponseQuery.builder()
                           .status(HttpStatus.ACCEPTED)
                           .code(HttpStatus.ACCEPTED.value())
                           .message("Add skills for collaborator")
                           .data(Map.of("Add Skill", serviceCollaborator.addCompetences(skillDTO)))
                           .build()
           );
       } catch (CollaboratorException e) {
           throw new CollaboratorException("Error encountered: "+e.getMessage()+ " caused: ", e.getCause());
       }
    }
}
