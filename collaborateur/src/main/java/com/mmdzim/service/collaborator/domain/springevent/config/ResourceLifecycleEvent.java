package com.mmdzim.service.collaborator.domain.springevent.config;

import org.springframework.context.ApplicationEvent;

import java.time.Clock;

public class ResourceLifecycleEvent extends ApplicationEvent {
    private final Long resourceId;
    private final OperationType operationType;
    public ResourceLifecycleEvent(Object source, Long resourceId, OperationType operationType) {
        super(source);
        this.resourceId = resourceId;
        this.operationType = operationType;
    }



    public enum OperationType{
        CREATED,
        UPDATED,
        DELETED
    }
}
