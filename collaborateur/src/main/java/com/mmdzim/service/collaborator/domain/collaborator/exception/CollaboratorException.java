package com.mmdzim.service.collaborator.domain.collaborator.exception;

public class CollaboratorException extends RuntimeException {
    public CollaboratorException(String message){
        super(message);
    }
    public CollaboratorException(String message, Throwable cause){
        super(message, cause);
    }
}
