package com.mmdzim.service.collaborator.domain.collaborator.dto;


import com.mmdzim.service.collaborator.domain.poste.dto.ProfileDTO;
import com.mmdzim.service.collaborator.domain.skils.dto.SkillDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class CreatedCollaboratorDTO {

    private String firstName;
    private String lastName;
    private String birthday;
    private String nationality;
    private String socialSecurityNumber;
    private String region;
    private String postalCode;
    private String address;
    private Long profileId;
    private List<SkillDTO> skillDTOS;
}
