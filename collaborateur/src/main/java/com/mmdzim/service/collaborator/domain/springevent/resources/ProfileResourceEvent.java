package com.mmdzim.service.collaborator.domain.springevent.resources;

import com.mmdzim.service.collaborator.domain.springevent.config.ResourceLifecycleEvent;

public class ProfileResourceEvent extends ResourceLifecycleEvent {
    public ProfileResourceEvent(Object source, Long resourceId, OperationType operationType) {
        super(source, resourceId, operationType);
    }
}
