package com.mmdzim.service.collaborator.domain.collaborator.helper;

import lombok.Data;
import lombok.experimental.SuperBuilder;
import org.springframework.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;


@Data
@SuperBuilder
public class ResponseQuery {
    protected HttpStatus status;
    protected int code;
    protected String message;
    protected Map<?, ?> data ;

}
