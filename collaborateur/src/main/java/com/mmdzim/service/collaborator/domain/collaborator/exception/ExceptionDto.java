package com.mmdzim.service.collaborator.domain.collaborator.exception;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class ExceptionDto {
    private final int status;
    private Set<String> errors;
    public void addError(@NonNull final String errorMessage) {
        if (errors == null) {
            errors = new HashSet<>();
        }
        errors.add(errorMessage);
    }
}
