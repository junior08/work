package com.mmdzim.service.collaborator.domain.skils.mapper;


import com.mmdzim.service.collaborator.domain.skils.dto.SkillDTO;
import com.mmdzim.service.collaborator.domain.skils.entity.Skill;
import org.springframework.stereotype.Component;

@Component
public class SkillMapper {

    public Skill skillDtoToSkill(SkillDTO skillDTO){
        return  Skill.Builder
                .newBuilder()
                .name(skillDTO.getName())
                .build();
    }

    public SkillDTO skillToSkillDTO(Skill skill){
        SkillDTO dto = new SkillDTO();
        skill.setName(skill.getName());
        return  dto;

    }

}
