package com.mmdzim.service.collaborator.domain.collaborator.repository;

import com.mmdzim.service.collaborator.domain.collaborator.entity.SkillsCollaborator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface SkillCollaboratorRepository extends JpaRepository<SkillsCollaborator, Long> {
}
