package com.mmdzim.service.collaborator.domain.poste.mapper;


import com.mmdzim.service.collaborator.domain.poste.dto.ProfileDTO;
import com.mmdzim.service.collaborator.domain.poste.entity.Profile;
import com.mmdzim.service.collaborator.domain.poste.service.ServiceProfile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProfileMapper {


    public Profile dtoToProfileEntity(Long id){
      return null;
    }

    public Profile entityDtoToProfileEntity(Long entityDTO){
        return Profile.Builder.newBuilder()
                .id(entityDTO)
                .build();
    }
    public Profile entityDtoToProfileEntity(ProfileDTO profileDTO) {
        Profile profile = new Profile();
        profile.setName(profileDTO.getName());
        return profile;
    }

}
