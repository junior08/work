package com.mmdzim.service.collaborator.domain.springevent.publisher;

import org.springframework.lang.NonNull;

public interface EventPublisher<T> {
    void publishEvent(@NonNull T applicationEvent);
}
