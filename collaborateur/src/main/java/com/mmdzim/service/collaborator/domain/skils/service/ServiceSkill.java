package com.mmdzim.service.collaborator.domain.skils.service;

import com.mmdzim.service.collaborator.domain.collaborator.entity.Collaborator;
import com.mmdzim.service.collaborator.domain.skils.entity.Skill;

import java.util.List;

public interface ServiceSkill {
    Skill addSkill(Skill skill);
    Skill getSkill(Long id);
    List<Skill> getSkillsByCollaborator(Long idCollaborator);
    boolean checkSkill(String name);
    void deleteSkill(Long id);
    void addSkillForCollaborator(List<Skill> skills, Collaborator collaborator);
}
