package com.mmdzim.service.collaborator.domain.collaborator.service.impl;

import com.mmdzim.service.collaborator.domain.collaborator.entity.SkillsCollaborator;
import com.mmdzim.service.collaborator.domain.collaborator.repository.SkillCollaboratorRepository;
import com.mmdzim.service.collaborator.domain.collaborator.service.ServiceSkillCollaborator;
import com.mmdzim.service.collaborator.domain.skils.exception.SkillException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class ImpServiceSkillCollaborator implements ServiceSkillCollaborator {

    private final SkillCollaboratorRepository skillCollaboratorRepository;

    public ImpServiceSkillCollaborator(SkillCollaboratorRepository skillCollaboratorRepository) {
        this.skillCollaboratorRepository = skillCollaboratorRepository;
    }

    @Override
    public SkillsCollaborator add(SkillsCollaborator skillsCollaborator) {
        return  skillCollaboratorRepository.saveAndFlush(skillsCollaborator);
    }

    @Override
    public void deleteSkillsForCollaborator(Long id) {
        SkillsCollaborator skillsCollaborator =
                skillCollaboratorRepository.findById(id)
                        .orElseThrow(() ->
                                new SkillException("Don't found a relation between Skill and collaborator"));
        skillCollaboratorRepository.delete(skillsCollaborator);
    }
}
