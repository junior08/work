package com.mmdzim.service.collaborator.domain.collaborator.dto;

import com.mmdzim.service.collaborator.domain.collaborator.entity.Competences;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@SuperBuilder
public class AddSkillDTO {

    private Long collaboratorId;
    private List<Competences> competencesList;
}
