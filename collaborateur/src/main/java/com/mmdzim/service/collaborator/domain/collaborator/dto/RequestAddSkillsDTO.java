package com.mmdzim.service.collaborator.domain.collaborator.dto;


import com.mmdzim.service.collaborator.domain.skils.entity.Skill;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RequestAddSkillsDTO {

    private Long collaboratorId;
    private List<Skill> skills;
}
