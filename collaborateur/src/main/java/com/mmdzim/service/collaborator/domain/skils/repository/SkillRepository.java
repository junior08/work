package com.mmdzim.service.collaborator.domain.skils.repository;

import com.mmdzim.service.collaborator.domain.skils.entity.Skill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface SkillRepository extends JpaRepository<Skill, Long> {

    @Query(value = "SELECT * FROM skills s WHERE s.name = :name LIMIT 1", nativeQuery = true)
    Optional<Skill> findByName(String name);
}
