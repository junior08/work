package com.mmdzim.service.collaborator.domain.collaborator.dto;

import com.mmdzim.service.collaborator.domain.collaborator.entity.Collaborator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.List;





public class CollaboratorResponse extends AbstractCollaboratorDTO {
    private String profile;
    private List<String> skills;

    // constructors
    public CollaboratorResponse() {
    }
    public CollaboratorResponse(Long id, String firstName, String lastName, String birthday, String nationality,
                                String socialSecurityNumber, String address, String profile, List<String> skills) {
        super.setId(id);
        super.setFirstName(firstName);
        super.setLastName(lastName);
        super.setBirthday(birthday);
        super.setNationality(nationality);
        super.setSocialSecurityNumber(socialSecurityNumber);
        super.setAddress(address);
        this.profile = profile;
        this.skills = skills;
    }

    // getters and setters
    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public List<String> getSkills() {
        return skills;
    }

    public void setSkills(List<String> skills) {
        this.skills = skills;
    }
}
