package com.mmdzim.service.collaborator.domain.poste.repository;

import com.mmdzim.service.collaborator.domain.poste.entity.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface ProfileRepository extends JpaRepository<Profile, Long> {
    @Query(value = "SELECT * FROM profile WHERE name= :name", nativeQuery = true)
    Optional<Profile> getByName(String name);
}
