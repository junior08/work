package com.mmdzim.service.collaborator.domain.collaborator.service;

import com.mmdzim.service.collaborator.domain.collaborator.entity.SkillsCollaborator;

public interface ServiceSkillCollaborator {
    SkillsCollaborator add(SkillsCollaborator skillsCollaborator);
    void deleteSkillsForCollaborator(Long id);
}
