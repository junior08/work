package com.mmdzim.service.collaborator.domain.collaborator.service.impl;

import com.mmdzim.service.collaborator.domain.collaborator.dto.AddSkillDTO;
import com.mmdzim.service.collaborator.domain.collaborator.dto.CollaboratorResponse;
import com.mmdzim.service.collaborator.domain.collaborator.dto.CreatedCollaboratorDTO;
import com.mmdzim.service.collaborator.domain.collaborator.dto.ResponseGetOneCollaboratorDTO;
import com.mmdzim.service.collaborator.domain.collaborator.entity.Collaborator;
import com.mmdzim.service.collaborator.domain.collaborator.entity.Competences;
import com.mmdzim.service.collaborator.domain.collaborator.entity.SkillsCollaborator;
import com.mmdzim.service.collaborator.domain.collaborator.exception.CollaboratorException;
import com.mmdzim.service.collaborator.domain.collaborator.exception.CollaboratorNotFoundException;
import com.mmdzim.service.collaborator.domain.collaborator.mapper.CollaboratorMapper;
import com.mmdzim.service.collaborator.domain.collaborator.repository.CollaboratorRepository;
import com.mmdzim.service.collaborator.domain.collaborator.service.ServiceCollaborator;
import com.mmdzim.service.collaborator.domain.collaborator.service.ServiceSkillCollaborator;
import com.mmdzim.service.collaborator.domain.poste.entity.Profile;
import com.mmdzim.service.collaborator.domain.poste.exception.ProfileNotFoundException;
import com.mmdzim.service.collaborator.domain.poste.service.ServiceProfile;
import com.mmdzim.service.collaborator.domain.skils.entity.Skill;
import com.mmdzim.service.collaborator.domain.skils.service.ServiceSkill;
import com.mmdzim.service.collaborator.domain.springevent.config.ResourceLifecycleEvent;
import com.mmdzim.service.collaborator.domain.springevent.publisher.EventPublisher;
import com.mmdzim.service.collaborator.domain.springevent.resources.CollaboratorResourceEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Service
@Slf4j
@Transactional
public class ImplServiceCollaborator implements ServiceCollaborator {

    public final CollaboratorRepository collaboratorRepository;
    public final CollaboratorMapper collaboratorMapper;
    public final ServiceProfile serviceProfile;
    private final ServiceSkill serviceSkill;
    private final EventPublisher<ApplicationEvent> localEventPublisher;

    @Autowired
    private ServiceSkillCollaborator serviceSkillCollaborator;


    public ImplServiceCollaborator(CollaboratorRepository collaboratorRepository,
                                   CollaboratorMapper collaboratorMapper,
                                   ServiceProfile serviceProfile, ServiceSkill serviceSkill, EventPublisher<ApplicationEvent> localEventPublisher) {
        this.collaboratorRepository = collaboratorRepository;
        this.collaboratorMapper = collaboratorMapper;
        this.serviceProfile = serviceProfile;
        this.serviceSkill = serviceSkill;
        this.localEventPublisher = localEventPublisher;
    }
    @Override
    public Collaborator createCollaborator(CreatedCollaboratorDTO collaboratorDTO) {
        try{
            log.info("Creation collaborator");
            Profile profile = serviceProfile.findById(collaboratorDTO.getProfileId());
           if(profile.getId() != null) {

               Collaborator collaborator = collaboratorRepository.save(
                       collaboratorMapper.collaboratorDtoToCollaboratorEntity(collaboratorDTO, profile)
               );
               localEventPublisher.publishEvent(
                   new CollaboratorResourceEvent(
                           this,
                           collaborator.getId(),
                           ResourceLifecycleEvent.OperationType.CREATED
                   )
               );
               log.info("This event: {} is publishing", localEventPublisher);
               return  collaborator;
           }
           throw new ProfileNotFoundException("Can't find a Profile with this ID: "+ collaboratorDTO.getProfileId());

        }
        catch (CollaboratorException e){
            throw  new CollaboratorException("Error :", e.getCause());
        }
    }

    @Override
    public Page<Collaborator> listCollaborator(Pageable pageable) {
        try {
            return collaboratorRepository.findAll(pageable);
        }catch (CollaboratorException e){
            throw  new CollaboratorException("Error :", e.getCause());
        }
    }

    @Override
    public Collaborator getOneCollaborator(Long id) {
            Optional<Collaborator> collaborator = collaboratorRepository.findById(id);
            return collaborator.orElseThrow(() -> new CollaboratorNotFoundException("Collaborator not found"));
    }

    @Override
    public void deleteCollaborator(Long id) {
        if(getOneCollaborator(id).getId()!= null) {
            collaboratorRepository.deleteById(id);
        }
    }
    @Override
    public List<Collaborator> findByProfil(String profile) {
        Optional<List<Collaborator>> collaborator = collaboratorRepository.getCollaboratorByProfileName(profile);
        return collaborator.orElseThrow(()
                -> new CollaboratorNotFoundException("Can't found one collaborator with the profile"));
    }
    @Override
    public Collaborator addSkillToCollaborator(List<Skill> list, Long collaboratorId) {
        Collaborator collaborator = getOneCollaborator(collaboratorId);
            for (Skill item: list) {
                addAndSaveSkills(item, collaborator);
            }
            return  collaborator;
    }
    private void addAndSaveSkills(Skill item, Collaborator collaborator){
        Skill skill = serviceSkill.getSkill(item.getId());
        if(skill.getId()!= null) {
            SkillsCollaborator skillsCollaborator =
                    SkillsCollaborator.Builder.
                    newBuilder()
                    .collaborator(collaborator)
                    .skill(skill)
                    .build();
            serviceSkillCollaborator.add(skillsCollaborator);
            collaborator.getSkillsCollaborators().add(skillsCollaborator);
            collaboratorRepository.saveAndFlush(collaborator);
        }

    }



    @Override
    public void addOrUpdatePoste(Long collaboratorId, Long profileId) {
        Collaborator collaborator = getOneCollaborator(collaboratorId);
        if(collaborator.getId() != null) {
            collaborator.setProfile(serviceProfile.findById(profileId));
            collaboratorRepository.save(collaborator);
        }
    }

    @Override
    public List<Collaborator>   findAll() {
        return collaboratorRepository.findAll();
    }

    @Override
    public ResponseGetOneCollaboratorDTO findOne(Long id) {
        Optional<Collaborator> collaborator = collaboratorRepository.findById(id);
        return collaboratorMapper.CollaboratorToResponseGetOneCollaboratorDTO(
                collaborator.orElseThrow(() ->
                        new CollaboratorNotFoundException("Can't found one collaborator with the profile"))
        );
    }

    @Override
    public CollaboratorResponse getOne(Long id) {
        Optional<Collaborator> collaborator = collaboratorRepository.findById(id);
        return collaboratorMapper.CollaboratoToCollaboratorResponse(
                collaborator.orElseThrow(() -> new CollaboratorNotFoundException("Can't found one collaborator with the profile"))
        );
    }

    @Override
    public Collaborator addCompetences(AddSkillDTO skillDTO) {
        Collaborator collaborator = getOneCollaborator(skillDTO.getCollaboratorId());
        collaborator.setCompetences((Competences) skillDTO.getCompetencesList());
        return collaborator;
    }

    @Override
    public Collaborator updateOrRemoveCompetences(AddSkillDTO skillDTO) {
        return null;
    }
}
