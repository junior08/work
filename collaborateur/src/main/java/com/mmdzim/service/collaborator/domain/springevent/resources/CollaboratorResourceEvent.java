package com.mmdzim.service.collaborator.domain.springevent.resources;

import com.mmdzim.service.collaborator.domain.springevent.config.ResourceLifecycleEvent;

public class CollaboratorResourceEvent extends ResourceLifecycleEvent {
    public CollaboratorResourceEvent(Object source, Long resourceId, OperationType operationType) {
        super(source, resourceId, operationType);

    }
}
