package com.mmdzim.service.collaborator.domain.collaborator.repository;

import com.mmdzim.service.collaborator.domain.collaborator.entity.Collaborator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface CollaboratorRepository extends JpaRepository<Collaborator, Long> {

    @Query(value = "SELECT c.* FROM Collaborator c JOIN Profile p " +
            "ON c.profile_id = p.id WHERE p.name LIKE `profileName%`", nativeQuery = true)
    Optional<List<Collaborator>> getCollaboratorByProfileName(String profileName);
}
