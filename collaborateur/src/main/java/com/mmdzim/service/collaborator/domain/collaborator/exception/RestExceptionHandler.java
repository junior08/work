package com.mmdzim.service.collaborator.domain.collaborator.exception;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler {
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ExceptionDto> handleValidationExceptions(final MethodArgumentNotValidException ex) {
        final var responseFormat = "'%s' %s";
        final var errorResponse = new ExceptionDto(400);
        ex.getBindingResult().getAllErrors().forEach(error -> {
            final var message =  String.format(responseFormat,
                    ((FieldError) error).getField(),
                    error.getDefaultMessage() != null ? error.getDefaultMessage() : ""
            ).trim();
            errorResponse.addError(message);
        });

        return new ResponseEntity<>(
                errorResponse,
                HttpStatus.valueOf(errorResponse.getStatus())
        );
    }
}
