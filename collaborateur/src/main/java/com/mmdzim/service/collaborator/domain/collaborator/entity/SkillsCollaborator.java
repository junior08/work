package com.mmdzim.service.collaborator.domain.collaborator.entity;


import com.mmdzim.service.collaborator.domain.skils.entity.Skill;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "skill_collaborators")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SkillsCollaborator {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "collaborator_id", nullable = false)
    private Collaborator collaborator;
    @ManyToOne
    @JoinColumn(name = "skill_id", nullable = false)
    private Skill skill;

    private SkillsCollaborator(Builder builder) {
        setId(builder.id);
        setCollaborator(builder.collaborator);
        setSkill(builder.skill);
    }

    public static final class Builder {
        private Long id;
        private Collaborator collaborator;
        private Skill skill;

        private Builder() {
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder id(Long val) {
            id = val;
            return this;
        }

        public Builder collaborator(Collaborator val) {
            collaborator = val;
            return this;
        }

        public Builder skill(Skill val) {
            skill = val;
            return this;
        }

        public SkillsCollaborator build() {
            return new SkillsCollaborator(this);
        }
    }
}
