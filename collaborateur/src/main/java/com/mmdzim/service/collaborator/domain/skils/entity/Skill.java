package com.mmdzim.service.collaborator.domain.skils.entity;

import com.mmdzim.service.collaborator.domain.collaborator.entity.SkillsCollaborator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "skills")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Skill {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @OneToMany(mappedBy = "skill")
    private List<SkillsCollaborator> skillsCollaborators;

    private Skill(Builder builder) {
        setId(builder.id);
        setName(builder.name);
    }


    public static final class Builder {
        private Long id;
        private String name;

        private Builder() {
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder id(Long val) {
            id = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Skill build() {
            return new Skill(this);
        }
    }
}
