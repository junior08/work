package com.mmdzim.service.collaborator.domain.exceptions;

public enum ErrorStatus {
    INTERNAL_SERVER,
    NOT_FOUND,
    ALREADY_EXIST
}
