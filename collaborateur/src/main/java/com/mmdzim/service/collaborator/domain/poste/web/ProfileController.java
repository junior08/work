package com.mmdzim.service.collaborator.domain.poste.web;


import com.mmdzim.service.collaborator.domain.collaborator.helper.ResponseQuery;
import com.mmdzim.service.collaborator.domain.poste.dto.ProfileDTO;
import com.mmdzim.service.collaborator.domain.poste.exception.ProfileException;
import com.mmdzim.service.collaborator.domain.poste.exception.ProfileNotFoundException;
import com.mmdzim.service.collaborator.domain.poste.service.ServiceProfile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("api/profile")
public class ProfileController {

    private final ServiceProfile serviceProfile;

    public ProfileController(ServiceProfile serviceProfile) {
        this.serviceProfile = serviceProfile;
    }


    @GetMapping("/{id}")
    public ResponseEntity<ResponseQuery> getProfile(@PathVariable("id" ) Long id) throws ProfileNotFoundException {
        try {
            return ResponseEntity.ok(
                    ResponseQuery.builder()
                            .code(HttpStatus.ACCEPTED.value())
                            .status(HttpStatus.ACCEPTED)
                            .message("")
                            .data(Map.of("Profile",serviceProfile.findById(id)))
                            .build()
            );
        }
        catch (ProfileNotFoundException e) {
            throw new ProfileNotFoundException("Not found profile", e.getCause());
        }

    }

    @PostMapping("/")
    public ResponseEntity<ResponseQuery> addProfile(@RequestBody ProfileDTO profileDTO) {
        try {
            return ResponseEntity.ok(
                    ResponseQuery.builder()
                            .code(HttpStatus.ACCEPTED.value())
                            .status(HttpStatus.ACCEPTED)
                            .message("")
                            .data(Map.of("Profile", serviceProfile.add(profileDTO)))
                            .build()
            );
        }catch (ProfileException e) {
            throw new ProfileException("Error encountered: "+ e.getMessage());
        }
    }
}
