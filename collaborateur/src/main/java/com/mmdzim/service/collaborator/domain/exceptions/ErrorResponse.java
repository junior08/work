package com.mmdzim.service.collaborator.domain.exceptions;

public class ErrorResponse {
    private ErrorStatus status;
    private String error;
    private int code;
    private String message;

    public ErrorResponse(ErrorStatus status, int code, String error, String  message ) {
        this.status = status;
        this.error = error;
        this.code = code;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }
    public void setCode(int code) {
        this.code = code;
    }

    public ErrorStatus getStatus() {
        return status;
    }

    public void setStatus(ErrorStatus status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
