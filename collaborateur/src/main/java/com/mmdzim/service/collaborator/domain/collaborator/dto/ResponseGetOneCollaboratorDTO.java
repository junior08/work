package com.mmdzim.service.collaborator.domain.collaborator.dto;


import com.mmdzim.service.collaborator.domain.skils.dto.SkillDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class ResponseGetOneCollaboratorDTO  {

    private Long id;
    private String firstName;
    private String lastName;
    private String birthday;
    private String nationality;
    private String socialSecurityNumber;
    private String profile;
    private List<String> skills;

}
