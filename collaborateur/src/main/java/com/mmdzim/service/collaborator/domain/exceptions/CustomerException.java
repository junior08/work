package com.mmdzim.service.collaborator.domain.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;

import static com.mmdzim.service.collaborator.domain.exceptions.ExceptionConstants.*;


//@RestControllerAdvice
public class CustomerException {
    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse handleInternalServerError(RuntimeException ex) {
        return new ErrorResponse(ErrorStatus.INTERNAL_SERVER, INTERNAL_SERVER_CODE, INTERNAL_SERVER, ex.getMessage());
    }
    @ExceptionHandler(HttpClientErrorException.BadRequest.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleBadRequestError(HttpClientErrorException.BadRequest ex) {
        return new ErrorResponse(ErrorStatus.INTERNAL_SERVER, BAD_REQUEST_CODE, BAD_REQUEST, ex.getMessage());
    }
    @ExceptionHandler(HttpClientErrorException.NotFound.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorResponse handleNotFoundRequestError(HttpClientErrorException.NotFound ex) {
        return new ErrorResponse(ErrorStatus.INTERNAL_SERVER, NOT_FOUND_CODE, NOT_FOUND, ex.getMessage());
    }

}
