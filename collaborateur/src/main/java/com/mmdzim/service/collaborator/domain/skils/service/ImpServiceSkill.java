package com.mmdzim.service.collaborator.domain.skils.service;

import com.mmdzim.service.collaborator.domain.collaborator.entity.Collaborator;
import com.mmdzim.service.collaborator.domain.skils.entity.Skill;
import com.mmdzim.service.collaborator.domain.skils.exception.SkillException;
import com.mmdzim.service.collaborator.domain.skils.repository.SkillRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Service
@Transactional
@Slf4j
public class ImpServiceSkill implements ServiceSkill {

    private final SkillRepository skillRepository;

    public ImpServiceSkill(SkillRepository skillRepository) {
        this.skillRepository = skillRepository;
    }

    @Override
    public Skill addSkill(Skill skill) {
        log.info("Add new skill operation");
        if(!checkSkill(skill.getName())) {
            throw new SkillException("Skill with this name:" +skill.getName()+ " already exist in database");
        }
        return skillRepository.save(skill);
    }

    @Override
    public Skill getSkill(Long id) {
        log.info("Search skill by id:{}", id);
        return skillRepository.findById(id).orElseThrow(() -> new SkillException("Can't found Skill with id: "+ id));
    }

    @Override
    public List<Skill> getSkillsByCollaborator(Long idCollaborator) {
        return null;
    }

    @Override
    public boolean checkSkill(String name) {
        return skillRepository.findByName(name).isPresent();
    }

    @Override
    public void deleteSkill(Long id) {

    }

    @Override
    public void addSkillForCollaborator(List<Skill> skills, Collaborator collaborator) {
       for (Skill item: skills) {
           if(!checkSkill(item.getName())) {
               Skill skill = skillRepository.save(item);

           }
       }
    }
}
