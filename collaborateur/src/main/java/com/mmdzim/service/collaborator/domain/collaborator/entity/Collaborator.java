package com.mmdzim.service.collaborator.domain.collaborator.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mmdzim.service.collaborator.domain.collaborator.mapper.CompetenceConverter;
import com.mmdzim.service.collaborator.domain.poste.entity.Profile;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "collaborator")
@Getter
@Setter
public class Collaborator {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private String firstName;
    @Column(nullable = false)
    private String lastName;
    @Column(nullable = false)
    private String birthday;
    @Column(nullable = false)
    private String nationality;
    @Column(unique = true, updatable = false, nullable = false)
    private String socialSecurityNumber;
    private String region;
    @Column(nullable = false)
    private String postalCode;
    @Column(nullable = false)
    private String address;
    @Column(nullable = false)
    private boolean available;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name ="profile_id", nullable = false)
    private Profile profile;
    @OneToMany(mappedBy = "collaborator", fetch = FetchType.EAGER)
    @JsonIgnore
    private List<SkillsCollaborator> skillsCollaborators;

    @Convert(converter = CompetenceConverter.class)
    private Competences competences;
}
