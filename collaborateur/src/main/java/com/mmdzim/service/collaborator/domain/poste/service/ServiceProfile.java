package com.mmdzim.service.collaborator.domain.poste.service;

import com.mmdzim.service.collaborator.domain.poste.dto.ProfileDTO;
import com.mmdzim.service.collaborator.domain.poste.entity.Profile;

public interface ServiceProfile {
    Profile findById(Long id);
    Profile findByName(String name);

    Profile add(ProfileDTO profileDTO);
}
