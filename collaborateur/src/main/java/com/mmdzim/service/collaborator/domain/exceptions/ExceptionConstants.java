package com.mmdzim.service.collaborator.domain.exceptions;

public class ExceptionConstants {
    private ExceptionConstants(){}

    public static String INTERNAL_SERVER = "Internal Server Error";
    public static int INTERNAL_SERVER_CODE = 500;
    public static String NOT_FOUND = "Can't found item";
    public static int NOT_FOUND_CODE = 404;
    public static String BAD_REQUEST = "Bad request error";
    public static int BAD_REQUEST_CODE = 400;
}
