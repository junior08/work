package com.mmdzim.service.collaborator.domain.collaborator.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;



@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class ResponseDTOCollaborator  {
    public Long id;
    public String firstName;
    public String lastName;
    public String birthday;
    public String nationality;
    public String socialSecurityNumber;



}
