package com.mmdzim.service.collaborator.domain.collaborator.mapper;

import com.mmdzim.service.collaborator.domain.collaborator.dto.CollaboratorResponse;
import com.mmdzim.service.collaborator.domain.collaborator.dto.CreatedCollaboratorDTO;
import com.mmdzim.service.collaborator.domain.collaborator.dto.ResponseGetOneCollaboratorDTO;
import com.mmdzim.service.collaborator.domain.collaborator.entity.Collaborator;
import com.mmdzim.service.collaborator.domain.poste.entity.Profile;
import com.mmdzim.service.collaborator.domain.poste.mapper.ProfileMapper;
import com.mmdzim.service.collaborator.domain.skils.dto.SkillDTO;
import com.mmdzim.service.collaborator.domain.skils.mapper.SkillMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;


@Component
@Slf4j
public class CollaboratorMapper {
    private final ProfileMapper profileMapper;
    private final SkillMapper skillMapper;
    public CollaboratorMapper(ProfileMapper profileEntityMapper, SkillMapper skillMapper) {
        this.profileMapper = profileEntityMapper;
        this.skillMapper = skillMapper;
    }

    public Collaborator
    collaboratorDtoToCollaboratorEntity(CreatedCollaboratorDTO collaboratorDTO, Profile profile){
        Collaborator entity = new Collaborator();
        entity.setAddress(collaboratorDTO.getAddress());
        entity.setProfile(profile);
        entity.setAvailable(true);
        entity.setRegion(collaboratorDTO.getRegion());
        entity.setFirstName(collaboratorDTO.getFirstName());
        entity.setLastName(collaboratorDTO.getLastName());
        entity.setBirthday(collaboratorDTO.getBirthday());
        entity.setNationality(collaboratorDTO.getNationality());
        entity.setSocialSecurityNumber(collaboratorDTO.getSocialSecurityNumber());
        entity.setPostalCode(collaboratorDTO.getPostalCode());
        return  entity;
    }

    public ResponseGetOneCollaboratorDTO CollaboratorToResponseGetOneCollaboratorDTO(Collaborator collaborator){
        List<String> list =
                collaborator.getSkillsCollaborators()
                        .stream().map(
                                skillsCollaborator -> skillsCollaborator.getSkill().getName()
                        ).collect(Collectors.toList());
        log.info("Liste: {}", list);
        ResponseGetOneCollaboratorDTO dto = new ResponseGetOneCollaboratorDTO();
        dto.setFirstName(collaborator.getFirstName());
        dto.setLastName(collaborator.getLastName());
        dto.setNationality(collaborator.getNationality());
        dto.setId(collaborator.getId());
        dto.setSocialSecurityNumber(collaborator.getSocialSecurityNumber());
        dto.setSkills(list);
        dto.setProfile(collaborator.getProfile().getName());
        dto.setBirthday(collaborator.getBirthday());
        return dto;
    }

    public Collaborator setProfileCollaboratorByProfileId(Long idCollaborator, Long profileId){
        return null;
    }
    public CollaboratorResponse CollaboratoToCollaboratorResponse(Collaborator collaborator){
        CollaboratorResponse response = new CollaboratorResponse();
        response.setId(collaborator.getId());
        response.setFirstName(collaborator.getFirstName());
        response.setLastName(collaborator.getLastName());
        response.setAddress(collaborator.getAddress());
        response.setNationality(collaborator.getNationality());
        response.setBirthday(collaborator.getBirthday());
        response.setSocialSecurityNumber(collaborator.getSocialSecurityNumber());
        response.setProfile(collaborator.getProfile().getName());
        response.setSkills(
                collaborator.getSkillsCollaborators().stream().map(
                        skillsCollaborator -> skillsCollaborator.getSkill().getName()
                ).collect(Collectors.toList())
        );
        return response;
    }

}
