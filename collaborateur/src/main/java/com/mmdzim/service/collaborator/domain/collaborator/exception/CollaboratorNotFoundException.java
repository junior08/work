package com.mmdzim.service.collaborator.domain.collaborator.exception;

public class CollaboratorNotFoundException extends RuntimeException{
    public CollaboratorNotFoundException(String message){
        super(message);
    }
    public CollaboratorNotFoundException(String message, Throwable cause){
        super(message, cause);
    }
}
