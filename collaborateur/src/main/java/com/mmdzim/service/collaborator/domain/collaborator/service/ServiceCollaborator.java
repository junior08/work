package com.mmdzim.service.collaborator.domain.collaborator.service;

import com.mmdzim.service.collaborator.domain.collaborator.dto.*;
import com.mmdzim.service.collaborator.domain.collaborator.entity.Collaborator;
import com.mmdzim.service.collaborator.domain.collaborator.entity.Competences;
import com.mmdzim.service.collaborator.domain.skils.entity.Skill;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;



public interface ServiceCollaborator {

    Collaborator createCollaborator(CreatedCollaboratorDTO collaboratorDTO);
    Page<Collaborator> listCollaborator(Pageable pageable);
    Collaborator getOneCollaborator(Long uuid);
    void deleteCollaborator(Long uuid);
    List<Collaborator> findByProfil(String profile);
    Collaborator addSkillToCollaborator(List<Skill> list, Long collaboratorId);

    void addOrUpdatePoste(Long collaboratorId, Long profileId);
    List<Collaborator> findAll();

    ResponseGetOneCollaboratorDTO findOne(Long id);

    CollaboratorResponse getOne(Long id);

    Collaborator addCompetences(AddSkillDTO skillDTO);
    Collaborator updateOrRemoveCompetences(AddSkillDTO skillDTO);

}
