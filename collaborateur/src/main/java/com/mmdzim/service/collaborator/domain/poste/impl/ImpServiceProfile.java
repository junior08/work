package com.mmdzim.service.collaborator.domain.poste.impl;

import com.mmdzim.service.collaborator.domain.poste.dto.ProfileDTO;
import com.mmdzim.service.collaborator.domain.poste.entity.Profile;
import com.mmdzim.service.collaborator.domain.poste.exception.ProfileException;
import com.mmdzim.service.collaborator.domain.poste.exception.ProfileNotFoundException;
import com.mmdzim.service.collaborator.domain.poste.mapper.ProfileMapper;
import com.mmdzim.service.collaborator.domain.poste.repository.ProfileRepository;
import com.mmdzim.service.collaborator.domain.poste.service.ServiceProfile;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
@Slf4j
public class ImpServiceProfile implements ServiceProfile {

    private final ProfileRepository profileEntityRepository;
    private final ProfileMapper profileMapper;

    public ImpServiceProfile(ProfileRepository profileEntityRepository, ProfileMapper profileMapper) {
        this.profileEntityRepository = profileEntityRepository;
        this.profileMapper = profileMapper;
    }

    @Override
    public Profile findById(Long id) {
        Optional<Profile> profile = profileEntityRepository.findById(id);
        return profile.orElseThrow(() -> new ProfileNotFoundException("Can't Found Profile with id: "+id));
    }

    @Override
    public Profile findByName(String name) {
        Optional<Profile> profile = profileEntityRepository.getByName(name);
        return profile.orElseThrow(() -> new ProfileNotFoundException("Can't Found Profile with name: "+name));
    }

    @Override
    public Profile add(ProfileDTO profileDTO) {
        Optional<Profile> profile = profileEntityRepository.getByName(profileDTO.getName());
        if(profile.isPresent()) throw new ProfileException("One profile with name already exist");
        else {
            return profileEntityRepository.save(
                    profileMapper.entityDtoToProfileEntity(profileDTO)
            );
        }
    }
}
